# Пример crud приложения с вебсервером

## Бэкенд
 
Работа будет вестись с приложением user_app в app/cmd/user_app. Приложение можно собрать и отправить по scp командами в makefile, который также находится в директории app. Для команды scp надо подставить свой ip.

Приложение необходимо собрать, отправить на бэкенд сервер и открыть соответствующий порт. На бэкенд виртуальной машине нужно прописать переменные окружения:

```bash
export DB_TYPE=<postgres/mongodb>
export DB_HOST=<внутренний ip платформенной базы данных>
export DB_PORT=<порт, обычно 5432>
export DB_USER=<имя пользователя postgres>
export DB_PASSWORD=<пароль от базы данных>
export DB_NAME=<имя базы данных>
export PORT=<номер порта приложения, его нужно будет указать в index.html для веб сервера вместе с айпи бэкенда, также нужно прописать его в security group вм>
```

**Бэкенд поддерживает crud запросы:**

Hello сообщение для проверки жизнеспособности сервиса:
```bash
curl <ip бэкенда>:<порт>/info/hello
```
вывод:
```shell
{"message":"hello message"}
```

Получение списка пользователей:
```bash
curl <ip бэкенда>:<порт>/info/users
```
вывод:
```shell
[{"id":1,"name":"Maxim","location":"Moscow","age":21},{"id":2,"name":"Dima","location":"Moscow","age":22}]
```

Получение пользователя по id:
```bash
curl <ip бэкенда>:<порт>/info/user/{id}
```
вывод:
```shell
{"id":1,"name":"Maxim","location":"Moscow","age":21}
```

Добавить пользователя:
```bash
curl -X POST -H "Content-Type: application/json" -d '{
"name": "Dima",
"location": "Moscow",
"age": 22
}' http://<ip бэкенда>:<порт>/user-storage/user
```
вывод:
```shell
{"id":2,"message":"User created successfully"}
```

Обновить данные пользователя:
```bash
curl -X PATCH -H "Content-Type: application/json" -d '{
"name": "Dima",
"location": "Moscow",
"age": 22
}' http://<ip бэкенда>:<порт>/user-storage/user/{userId}
```
вывод:
```shell
{"id":2,"message":"User created successfully"}
```

Удалить пользователя:
```bash
curl -X DELETE <ip бэкенда>:<порт>/user-storage/user/{userId}
```
вывод:
```shell
{"id":1,"message":"User updated successfully. Total rows/record affected 1"}
```

## Фронтенд

После поднятия бэкенд сервера поднимается nginx на ubuntu 22.04:

```bash
sudo apt update
```

```bash
sudo apt install nginx
```

редактируем содержимое файлов на фронтенд сервере:

**/var/www/html/index.html** - копируем в этот файл содержимое  **frontend/index.html**

на 74 строке меняем на значения бэкенда

```html
const apiBaseUrl = 'http://<ip сервера где резмещён публично доступный бэкенд>/api'

**/etc/nginx/nginx.conf** - копируем в этот файл содержимое **frontend/nginx.conf**

**/etc/nginx/sites-available/default** - копируем в этот файл содержимое **frontend/default**

на 11 строке меняем на значения бэкенда

```html
        proxy_pass http://<ip сервера где резмещён публично доступный бэкенд>:<порт приложения>/;
```

перезапускаем nginx

```bash
sudo systemctl restart nginx.service
```

Приложение будет доступно по внешнему ip фронтенд сервера.

![web page](media/webpage.png)

Раз в 3 секунды обновляется таблица.