package main

import (
	"context"
	"fmt"
	"log"
	"mcs-kubernetes-project/internal/middleware"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/gorilla/mux"
)

func corsHandler(fn http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, req *http.Request) {
		rw.Header().Set("Access-Control-Allow-Origin", "*")
		rw.Header().Set("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE")
		rw.Header().Set("Access-Control-Allow-Headers", "Content-Type, Authorization")
		if req.Method == "OPTIONS" {
			rw.WriteHeader(http.StatusOK)
			return
		}
		fn(rw, req)
	}
}

func main() {
	// Initialize the database connection based on the environment variable
	dbType := os.Getenv("DB_TYPE")
	var db middleware.Database

	if dbType == "postgres" {
		postgresDB, err := middleware.CreatePostgresConnection()
		if err != nil {
			log.Fatalf("Could not connect to PostgreSQL: %v", err)
		}
		defer postgresDB.Close() // Close PostgreSQL connection
		db = postgresDB
	} else if dbType == "mongodb" {
		mongoDB, err := middleware.CreateMongoConnection()
		if err != nil {
			log.Fatalf("Could not connect to MongoDB: %v", err)
		}
		defer mongoDB.Close() // Close MongoDB connection
		db = mongoDB
	} else {
		log.Fatalf("Unsupported DB type: %v", dbType)
	}

	// Pass the db connection to the middleware package
	middleware.SetDB(db)

	// Setting up the router
	router := mux.NewRouter()

	router.HandleFunc("/info/version", corsHandler(middleware.Version)).Methods("GET", "OPTIONS")
	router.HandleFunc("/info/user/{id}", corsHandler(middleware.GetUser)).Methods("GET", "OPTIONS")
	router.HandleFunc("/info/users", corsHandler(middleware.GetUsers)).Methods("GET", "OPTIONS")
	router.HandleFunc("/info/hello", corsHandler(middleware.Hello)).Methods("GET", "OPTIONS")
	router.HandleFunc("/user-storage/user", corsHandler(middleware.CreateUser)).Methods("POST", "OPTIONS")
	router.HandleFunc("/user-storage/user/{id}", corsHandler(middleware.UpdateUser)).Methods("PUT", "OPTIONS")
	router.HandleFunc("/user-storage/user/{id}", corsHandler(middleware.DeleteUser)).Methods("DELETE", "OPTIONS")

	server := &http.Server{
		Addr:    fmt.Sprintf(":%s", os.Getenv("PORT")),
		Handler: router,
	}

	// Graceful shutdown
	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		log.Printf("Starting server on port %s...\n", os.Getenv("PORT"))
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("Could not listen on %s: %v\n", os.Getenv("PORT"), err)
		}
	}()

	<-stop // wait for interrupt signal
	log.Println("Shutting down server...")

	// Graceful shutdown logic
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	if err := server.Shutdown(ctx); err != nil {
		log.Fatalf("Server forced to shutdown: %v", err)
	}

	log.Println("Server exiting")
}
