package middleware

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"mcs-kubernetes-project/internal/models"
	"net/http"
	"os"
	"strconv"
	"time"

	_ "github.com/lib/pq"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

// response struct for API responses
type response struct {
	ID      int64  `json:"id,omitempty"`
	Message string `json:"message,omitempty"`
}

// Database interface for both PostgreSQL and MongoDB
type Database interface {
	InsertUser(user models.User) (int64, error)
	GetUser(id int64) (models.User, error)
	GetAllUsers() ([]models.User, error)
	UpdateUser(id int64, user models.User) (int64, error)
	DeleteUser(id int64) (int64, error)
	Close() error // Close database connections
}

// Global db variable
var db Database

// SetDB assigns the database instance
func SetDB(database Database) {
	db = database
}

// PostgreSQL implementation
type PostgresDB struct {
	Conn *sql.DB
}

func CreatePostgresConnection() (*PostgresDB, error) {
	dbportStr := os.Getenv("DB_PORT")
	dbport, err := strconv.Atoi(dbportStr)
	if err != nil {
		return nil, fmt.Errorf("unable to convert the string into int: %v", err)
	}

	psqlInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=enable",
		os.Getenv("DB_HOST"), dbport, os.Getenv("DB_USER"), os.Getenv("DB_PASSWORD"), os.Getenv("DB_NAME"))

	db, err := sql.Open("postgres", psqlInfo)
	if err != nil {
		return nil, fmt.Errorf("unable to open connection: %v", err)
	}

	err = db.Ping()
	if err != nil {
		return nil, fmt.Errorf("unable to ping database: %v", err)
	}

	log.Println("Successfully connected to PostgreSQL!")
	return &PostgresDB{Conn: db}, nil
}

func (p *PostgresDB) InsertUser(user models.User) (int64, error) {
	sqlStatement := `INSERT INTO users (name, location, age) VALUES ($1, $2, $3) RETURNING userid`
	var id int64
	err := p.Conn.QueryRow(sqlStatement, user.Name, user.Location, user.Age).Scan(&id)
	if err != nil {
		return 0, fmt.Errorf("unable to execute query: %v", err)
	}
	return id, nil
}

func (p *PostgresDB) GetUser(id int64) (models.User, error) {
	var user models.User
	sqlStatement := `SELECT * FROM users WHERE userid=$1`
	row := p.Conn.QueryRow(sqlStatement, id)
	err := row.Scan(&user.ID, &user.Name, &user.Location, &user.Age)
	if err == sql.ErrNoRows {
		return user, nil
	} else if err != nil {
		return user, fmt.Errorf("unable to scan row: %v", err)
	}
	return user, nil
}

func (p *PostgresDB) GetAllUsers() ([]models.User, error) {
	var users []models.User
	sqlStatement := `SELECT * FROM users`
	rows, err := p.Conn.Query(sqlStatement)
	if err != nil {
		return users, fmt.Errorf("unable to execute query: %v", err)
	}
	defer rows.Close()

	for rows.Next() {
		var user models.User
		err = rows.Scan(&user.ID, &user.Name, &user.Location, &user.Age)
		if err != nil {
			return users, fmt.Errorf("unable to scan row: %v", err)
		}
		users = append(users, user)
	}
	return users, nil
}

func (p *PostgresDB) UpdateUser(id int64, user models.User) (int64, error) {
	sqlStatement := `UPDATE users SET name=$2, location=$3, age=$4 WHERE userid=$1`
	res, err := p.Conn.Exec(sqlStatement, id, user.Name, user.Location, user.Age)
	if err != nil {
		return 0, fmt.Errorf("unable to execute query: %v", err)
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("unable to check affected rows: %v", err)
	}
	return rowsAffected, nil
}

func (p *PostgresDB) DeleteUser(id int64) (int64, error) {
	sqlStatement := `DELETE FROM users WHERE userid=$1`
	res, err := p.Conn.Exec(sqlStatement, id)
	if err != nil {
		return 0, fmt.Errorf("unable to execute query: %v", err)
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return 0, fmt.Errorf("unable to check affected rows: %v", err)
	}
	return rowsAffected, nil
}

func (p *PostgresDB) Close() error {
	return p.Conn.Close()
}

// MongoDB implementation
type MongoDB struct {
	Client *mongo.Client
}

func CreateMongoConnection() (*MongoDB, error) {
	clientOptions := options.Client().ApplyURI(os.Getenv("MONGO_URI"))
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		return nil, fmt.Errorf("unable to connect to MongoDB: %v", err)
	}

	err = client.Ping(context.TODO(), nil)
	if err != nil {
		return nil, fmt.Errorf("unable to ping MongoDB: %v", err)
	}

	log.Println("Successfully connected to MongoDB!")
	return &MongoDB{Client: client}, nil
}

func (m *MongoDB) InsertUser(user models.User) (int64, error) {
	collection := m.Client.Database("userdb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	result, err := collection.InsertOne(ctx, user)
	if err != nil {
		return 0, fmt.Errorf("unable to insert user: %v", err)
	}
	id := result.InsertedID.(int64)
	return id, nil
}

func (m *MongoDB) GetUser(id int64) (models.User, error) {
	var user models.User
	collection := m.Client.Database("userdb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	err := collection.FindOne(ctx, bson.M{"_id": id}).Decode(&user)
	if err != nil {
		return user, fmt.Errorf("unable to get user: %v", err)
	}
	return user, nil
}

func (m *MongoDB) GetAllUsers() ([]models.User, error) {
	var users []models.User
	collection := m.Client.Database("userdb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	cursor, err := collection.Find(ctx, bson.M{})
	if err != nil {
		return users, fmt.Errorf("unable to get all users: %v", err)
	}
	defer cursor.Close(ctx)

	for cursor.Next(ctx) {
		var user models.User
		err := cursor.Decode(&user)
		if err != nil {
			return users, fmt.Errorf("unable to decode user: %v", err)
		}
		users = append(users, user)
	}
	return users, nil
}

func (m *MongoDB) UpdateUser(id int64, user models.User) (int64, error) {
	collection := m.Client.Database("userdb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := collection.UpdateOne(ctx, bson.M{"_id": id}, bson.D{
		{"$set", bson.D{{"name", user.Name}, {"location", user.Location}, {"age", user.Age}}},
	})
	if err != nil {
		return 0, fmt.Errorf("unable to update user: %v", err)
	}
	return res.ModifiedCount, nil
}

func (m *MongoDB) DeleteUser(id int64) (int64, error) {
	collection := m.Client.Database("userdb").Collection("users")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	res, err := collection.DeleteOne(ctx, bson.M{"_id": id})
	if err != nil {
		return 0, fmt.Errorf("unable to delete user: %v", err)
	}
	return res.DeletedCount, nil
}

func (m *MongoDB) Close() error {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	return m.Client.Disconnect(ctx)
}

// Handlers
func CreateUser(w http.ResponseWriter, r *http.Request) {
	var user models.User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Invalid input", http.StatusBadRequest)
		return
	}

	insertID, err := db.InsertUser(user)
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to create user: %v", err), http.StatusInternalServerError)
		return
	}

	res := response{
		ID:      insertID,
		Message: "User created successfully",
	}
	json.NewEncoder(w).Encode(res)
}

func GetUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid user ID", http.StatusBadRequest)
		return
	}

	user, err := db.GetUser(int64(id))
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to get user: %v", err), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(user)
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	users, err := db.GetAllUsers()
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to get users: %v", err), http.StatusInternalServerError)
		return
	}

	json.NewEncoder(w).Encode(users)
}

func UpdateUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid user ID", http.StatusBadRequest)
		return
	}

	var user models.User
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, "Invalid input", http.StatusBadRequest)
		return
	}

	updatedRows, err := db.UpdateUser(int64(id), user)
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to update user: %v", err), http.StatusInternalServerError)
		return
	}

	res := response{
		ID:      int64(id),
		Message: fmt.Sprintf("User updated successfully. Total rows/record affected %v", updatedRows),
	}
	json.NewEncoder(w).Encode(res)
}

func DeleteUser(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	id, err := strconv.Atoi(params["id"])
	if err != nil {
		http.Error(w, "Invalid user ID", http.StatusBadRequest)
		return
	}

	deletedRows, err := db.DeleteUser(int64(id))
	if err != nil {
		http.Error(w, fmt.Sprintf("Failed to delete user: %v", err), http.StatusInternalServerError)
		return
	}

	res := response{
		ID:      int64(id),
		Message: fmt.Sprintf("User deleted successfully. Total rows/record affected %v", deletedRows),
	}
	json.NewEncoder(w).Encode(res)
}

// Version handler
func Version(w http.ResponseWriter, r *http.Request) {
	version := os.Getenv("VERSION")
	if version == "" {
		version = "unknown"
	}

	res := response{
		Message: "Current version: " + version,
	}
	json.NewEncoder(w).Encode(res)
}

// Hello handler
func Hello(w http.ResponseWriter, r *http.Request) {
	helloMessage := "Hello, welcome to the user service API!"

	res := response{
		Message: helloMessage,
	}

	json.NewEncoder(w).Encode(res)
}
